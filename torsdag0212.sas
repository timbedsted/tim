* SAS code Thursday 2DEC2021 *;
proc sql;
delete from APDM.DATASET_SPECIFICATION where model_sk=76 ;
delete from APDM.MDL_EVL_LAST_RUN_AS_OF_DT_DTL where model_sk=76 ;
delete from APDM.MM_REPORT_SPECIFICATION where model_sk=76 ;
delete from APDM.MODEL_BIN_INFO_STAGING where model_sk=76 ;
*delete from APDM.MODEL_OUTPUT_COLUMN where model_sk=76 ;
quit;
proc sql;

delete from APDM.MODEL_RULE_DTLS where model_sk=76 ;
delete from APDM.MODEL_RULE_MASTER where model_sk=76 ;
delete from APDM.MODEL_SEGMENT_MASTER where model_sk=76 ;
delete from APDM.MODEL_X_ACT_OUTCOME_VAR where model_sk=76 ;
delete from APDM.MODEL_X_BIN_ANALYSIS_SCHEME where model_sk=76 ;
delete from APDM.MODEL_X_MODELING_ABT where model_sk=76 ;
delete from APDM.MODEL_X_SCORECARD_CHRSTC where model_sk=76 ;
delete from APDM.MODEL_X_SCR_INPUT_VARIABLE where model_sk=76 ;
quit;

proc sql;

*delete from APDM.SCORECARD_BIN_GROUP where model_sk=76 ;
delete from APDM.SCORING_MODEL where model_sk=76 ;
delete from APDM.MODEL_MASTER where model_sk=76 ;

quit;


* check scoring run in APDM *;
filename output filesrvc folderpath="/Public" filename= "model_output_column.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc sql;
select * from apdm.model_output_column;
quit;
/* ===========ACTUAL CODE========================*/
ods html close;
 