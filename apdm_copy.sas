/**********************************************************************************************
	NAME:	APDM_COPY.SAS
	DESC:	Creates a copy of APDM in SAS tables
***********************************************************************************************/
* Run in SAS Studio *;
cas;
caslib _all_   assign;
 

* Run in SAS code Execution *;
filename apdmxpt filesrvc folderpath="/Public" filename="apdm.xpt" debug=http recfm=n;
proc cport library=apdm file=apdmxpt;
run;

* Run in SAS Studio *;
filename apdmxpt filesrvc folderpath="/Public" filename= "apdm.xpt" debug=http recfm=n;
proc cimport library=casuser file=apdmxpt;
run;

