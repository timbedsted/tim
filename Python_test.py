==============================================  TRAINING CODE for SAS Studio Studio ================================================
import numpy as np
import pickle
import pandas as pd
import sklearn



from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
X, y= load_iris(return_X_y=True)
clf=LogisticRegression(random_state=0,max_iter=10000).fit(X,y)
clf.predict(X[:2,:])



clf.predict_proba(X[:2,:])
clf.score(X,y)



==============================================  TRAINING CODE for Mdodel Studio ================================================
from sklearn import ensemble

# Get full data with inputs + partition indicator
dm_input.insert(0, dm_partitionvar)
fullX = dm_inputdf.loc[:, dm_input]

# Dummy encode class variables
fullX_enc = pd.get_dummies(fullX, columns=dm_class_input, drop_first=True)

# Create X (features/inputs); drop partition indicator
X_enc = fullX_enc[fullX_enc[dm_partitionvar] == dm_partition_train_val]
X_enc = X_enc.drop(dm_partitionvar, 1)

# Create y (labels)
y = dm_traindf[dm_dec_target]

# Fit RandomForest model w/ training data
params = {'n_estimators': 100, 'max_depth': 20, 'min_samples_leaf': 5}
dm_model = ensemble.RandomForestClassifier(**params)
dm_model.fit(X_enc, y)
print(dm_model)

# Save VariableImportance to CSV
varimp = pd.DataFrame(list(zip(X_enc, dm_model.feature_importances_)), columns=['Variable Name', 'Importance'])
varimp.to_csv(dm_nodedir + '/rpt_var_imp.csv', index=False)

# Score full data
fullX_enc = fullX_enc.drop(dm_partitionvar, 1)
dm_scoreddf = pd.DataFrame(dm_model.predict_proba(fullX_enc), columns=['P_BAD0', 'P_BAD1'])


=========================  Scoring Code =============================================
# A score function of the following form should be provided:
# def score_record(var_1, var_2, var_3, var_4):
#   "Output: outvar_1, outvar_2"
#   <code line 1>
#   <code line 2 and so on>
#   return outvar_1, outvar_2
#
# Note that the pickle file saved at dm_pklpath in the Training Code editor can be opened with the following code:
# open(settings.pickle_path + dm_pklname)


==============================  R code =========================================================

training.data.raw <- read.csv("C:/TEMP/heart.csv")
 
data <- read.csv("C:/TEMP/heart.csv")

model <- glm(Survived ~.,family=binomial(link='logit'),data=train)
summary(model)



anova(model, test="Chisq")

library(pscl)
pR2(model)

fitted.results <- predict(model,newdata=subset(test,select=c(2,3,4,5,6,7,8)),type='response')
fitted.results <- ifelse(fitted.results > 0.5,1,0)
misClasificError <- mean(fitted.results != test$Survived)
print(paste('Accuracy',1-misClasificError))

library(ROCR)
p <- predict(model, newdata=subset(test,select=c(2,3,4,5,6,7,8)), type="response")
pr <- prediction(p, test$Survived)
prf <- performance(pr, measure = "tpr", x.measure = "fpr")
plot(prf)
auc <- performance(pr, measure = "auc")
auc <- auc@y.values[[1]]
auc