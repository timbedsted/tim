/**************************************************************************
	NAME:	RM_Analyse_autoexec.sas
	DESC: 	Setsup CAS libs and prepares SAS STudio for analysis to support
			RM modelling
***************************************************************************/

* CAS startup *;
cas;
caslib _all_ assign;
caslib _all_ list; 


proc freq data=rm_mdl.cs_data;
	weight _freq_; 
	where gb ne .;
	tables gb*(bureau ec_card loans )   / list missing;
run;


proc logistic data=rm_mdl.cs_data;
	weight _freq_; 
	where gb ne .;
	model gb(event='1')= age bureau  tmadd tmjob1 nmbloan ec_card inc1 income regn cash loans ;
run; 


proc casutil;
	droptable  casdata='cs_rejects' incaslib='rm_bkt' ;
quit;



/*  Fix score dataset */
data rm_bkt.cs_rejects(promote=yes);
	set cs_rejects;
	gb=0;
run;

