/*********************************************************************************************
	NAME:	dave_table_1000012.sas
	DESC:	Code to run in Job execution to remove source_table_sk=1000012  from RMdemo2 env.
**********************************************************************************************/

filename apdmxpt filesrvc folderpath="/Public" filename="apdm.xpt" debug=http recfm=n; 
 
 
proc cport library=apdm file=apdmxpt; 
run; 
 
 
* Deleting ACCOUNT_SNAPSHOT_BASE (ASP) *;
* STEP 1*;
proc sql; 
	delete * from apdm.source_table_master 
	where  source_table_sk=1000012
	;
quit;

* Step 2*;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;


* Step 3 *;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where  (source_column_sk)=(1000337)
	;
quit;

* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;
		 
* Step 4 *;
proc sql; 
	delete * from apdm.subset_table_join_condition 
	where   (right_column_sk)=(1000275)
	;
quit;

* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;

* Step 5 *;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where  (source_column_sk)=(1000302)
	;
quit;


* Step 6 
  (dim_attribute_value_sk)=(1000627) is still referenced from table "target_node_exprssion_x_value".;
proc sql; 
	delete * from apdm.target_node_exprssion_x_value 
	where    (dim_attribute_value_sk)=(1000627) 
	;
quit;
  


* Step 5 repeat *;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where  (source_column_sk)=(1000302)
	;
quit;  


* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;
  
  
* step 7
 (source_column_sk)=(1000314) is still referenced from table "source_dim_attrib_value_master".;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where  (source_column_sk)=(1000314) 
	;
quit;  


* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;

* Step 8 
  (source_column_sk)=(1000315) is still referenced from table "source_dim_attrib_value_master".;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where  (source_column_sk)=(1000315) 
	;
quit;  


* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;

* Step 9 
    (source_column_sk)=(1000316) is still referenced from table "source_dim_attrib_value_master".;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where      (source_column_sk)=(1000316) 
	;
quit;  


/*
data _null_;
	set casuser.source_column_master;
	where source_table_sk=1000012;
	put source_column_sk ','; 

run;
*/
* STEP 10
removing all columns connected to table *;
proc sql; 
	delete * from apdm.source_dim_attrib_value_master 
	where      (source_column_sk) in 
(1000343 ,
1000344 ,
1000345 ,
1000346 ,
1000347 ,
1000348 ,
1000349 ,
1000350 ,
1000351 ,
1000352 ,
1000353 ,
1000354 ,
1000355 ,
1000356 ,
1000357 ,
1000358 ,
1000286 ,
1000359 ,
1000313 ,
1000360 ,
1000361 ,
1000362 ,
1000337 ,
1000363 ,
1000685 ,
1000275 ,
1000276 ,
1000277 ,
1000278 ,
1000279 ,
1000280 ,
1000281 ,
1000282 ,
1000283 ,
1000284 ,
1000285 ,
1000287 ,
1000288 ,
1000289 ,
1000290 ,
1000291 ,
1000292 ,
1000293 ,
1000294 ,
1000295 ,
1000296 ,
1000297 ,
1000298 ,
1000299 ,
1000300 ,
1000301 ,
1000302 ,
1000303 ,
1000304 ,
1000305 ,
1000306 ,
1000307 ,
1000308 ,
1000309 ,
1000310 ,
1000311 ,
1000312 ,
1000314 ,
1000315 ,
1000316 ,
1000317 ,
1000318 ,
1000319 ,
1000320 ,
1000321 ,
1000322 ,
1000323 ,
1000324 ,
1000325 ,
1000326 ,
1000327 ,
1000328 ,
1000329 ,
1000330 ,
1000331 ,
1000332 ,
1000333 ,
1000334 ,
1000335 ,
1000336 ,
1000338 ,
1000339 ,
1000340 ,
1000341 ,
1000342 );    
;
quit;    



* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;

* Step 11 
DETAIL: Key (source_column_sk)=(1000339) is still referenced from table "source_column_x_usage".;
proc sql; 
	delete * from apdm.source_column_x_usage 
	where  (source_column_sk)=(1000339)
	;
quit;

* Step 2 repeat *;
proc sql; 
	delete * from apdm.source_column_master 
	where  (source_table_sk)=(1000012)
	;
quit;

* STEP 1 Repeat *;
proc sql; 
	delete * from apdm.source_table_master 
	where  source_table_sk=1000012
	;
quit;

* Step 13 
DETAIL: Key 
       (source_table_sk)=(1000012) is still referenced from table "source_table_x_time_frequency".;
proc sql; 
	delete * from apdm.source_table_x_time_frequency 
	where  source_table_sk=1000012
	;
quit;

* STEP 1 Repeat *;
proc sql; 
	delete * from apdm.source_table_master 
	where  source_table_sk=1000012
	;
quit;

* Step 14 
DETAIL: Key (source_table_sk)=(1000012) is still 
       referenced from table "source_table_x_usage".;
proc sql; 
	delete * from apdm.source_table_x_usage 
	where  source_table_sk=1000012
	;
quit;


* STEP 1 Repeat *;
proc sql; 
	delete * from apdm.source_table_master 
	where  source_table_sk=1000012
	;
quit;











 
 