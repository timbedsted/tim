
* Sletning af SOA and purposes *;
proc datasets lib=apdm details;
run;


filename output filesrvc folderpath="/Public" filename= "APDM_list.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc datasets lib=apdm details;
run;
/* ===========ACTUAL CODE========================*/
ods html close; 
  
filename output filesrvc folderpath="/Public" filename= "level_master.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc print data=apdm.level_master;
run;

/* ===========ACTUAL CODE========================*/
ods html close; 
  
filename output filesrvc folderpath="/Public" filename= "purpose_x_level.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc print data=apdm.purpose_x_level;
run;
/* ===========ACTUAL CODE========================*/
ods html close; 

* slet nedefra *;
proc sql;
	delete * 
	from apdm.purpose_x_level
	where level_sk in (1000038,1000039, 1000040)  ;
quit;

proc sql;
	delete *
	from apdm.level_key_column_dtl
	where level_sk in (1000038,1000039, 1000040)  ;
quit;

proc sql;
	delete * 
	from apdm.source_table_x_level
	where level_sk in (1000038,1000039, 1000040)  ;
quit;		  

proc sql;
	delete * 
	from apdm.level_key_column_dtl
	where level_sk in (1000038,1000039, 1000040)  ;
quit;		
proc sql;
	delete * 
	from apdm.level_master
	where level_sk in (1000038,1000039, 1000040)  ;
quit;

