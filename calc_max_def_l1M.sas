/****** DEBUG START *****************************
%let m_dabt_build_dt=31MAR2012;


data _null_;
	t="31Mar2012:23:59:59"dt;

	call symput ('m_dabt_build_dttm',t);
run;
%let m_fedsql_option=;
%let m_dabt_output_table_libref=RM_DBSC;
%let m_dabt_output_table_nm=TEST;
%let m_dabt_output_table_libref=CASUSER;
%let M_DABT_OUTPUT_TABLE_NM=TEST2; 
%LET M_DABT_SUBSET_RETAIN_KEY_TBL_NM=TEST3; 
%LET M_DABT_RETAINED_KEY_COLUMN_NM=CUSTOMER_RK;
%LET M_DABT_SUBSET_RETAIN_KEY_LIBREF=LIB_SCR;

/****** DEBUG END  ******************************/


%macro calc_max_def_l1M;
OPTIONS MPRINT  SYMBOLGEN;

/*Macro variable DABT_LOAD_USER_INPUT_DTTM contains the input datetime.*/
%let refer_dttm = &m_dabt_build_dttm.;
/*Macro variable DABT_LOAD_USER_INPUT_DT contains the input date.*/
%let refer_date = &m_dabt_build_dt.;

data _null_;
input_dttm = &refer_dttm.;
call symput ('refer_dttm',input_dttm);
run;

/*Error handling*/
%let job_rc = 0;
%let err = &syserr;
%if (&err gt &job_rc) %then
%let job_rc = &err;
/*End : Error handling*/
%if %sysfunc(exist(&m_dabt_output_table_libref..&m_dabt_output_table_nm.)) %then %do;
	proc fedsql &m_fedsql_option.; 
		drop table &m_dabt_output_table_libref..&m_dabt_output_table_nm.; 
	quit;
%end;

proc fedsql &m_fedsql_option.;
	create table &m_dabt_output_table_libref..&m_dabt_output_table_nm. as
	select 	&m_dabt_subset_retain_key_tbl_nm..&m_dabt_retained_key_column_nm,
			max(ACCOUNT_snapshot_BASE.default_cnt) AS CALC_I_MAX_SNP_DEF_CNT_L1M
	from &m_dabt_subset_retain_key_libref..&m_dabt_subset_retain_key_tbl_nm. as &m_dabt_subset_retain_key_tbl_nm.
	Inner join BANKCRFM.ACCOUNT_snapshot_BASE as ACCOUNT_snapshot_BASE
	on &m_dabt_subset_retain_key_tbl_nm..CUSTOMER_RK = ACCOUNT_SNAPSHOT_BASE.CUSTOMER_RK
	WHERE ACCOUNT_SNAPSHOT_BASE.PERIOD_LAST_DTTM = &refer_dttm
	group by &m_dabt_subset_retain_key_tbl_nm..CUSTOMER_RK, &m_dabt_subset_retain_key_tbl_nm..&m_dabt_retained_key_column_nm;
quit;

/*Error handling*/
%let err = &syserr;
%if (&err gt &job_rc) %then
%let job_rc = &err;
/*End : Error handling*/
/*If the external code was not executed successfully,
then setting the variable m_dabt_extrnl_calc_success_flg to N.
Application checks if the value of m_dabt_extrnl_calc_success_flg is N.
If yes, then it stops further execution.*/
%if &job_rc. gt 0 %then
%let m_dabt_extrnl_calc_success_flg = N;
%mend calc_max_def_l1M;
%calc_max_def_l1M;
