data heart;
set sashelp.heart;

if status='Dead' then y=1; else y=0;
run;

proc contents data=heart;
run;


proc freq data=heart;
	tables status*y /list missing;
run;


proc logistic data=heart;
	 class sex  Chol_Status ; 
	model y(event='1')= sex  height weight diastolic systolic smoking Chol_Status ;
run;
