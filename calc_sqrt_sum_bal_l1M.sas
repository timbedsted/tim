%macro calc_sqrt_sum_bal_l1M;
OPTIONS MPRINT  SYMBOLGEN;

/*Macro variable DABT_LOAD_USER_INPUT_DTTM contains the input datetime.*/
%let refer_dttm = &m_dabt_build_dttm.;
/*Macro variable DABT_LOAD_USER_INPUT_DT contains the input date.*/
%let refer_date = &m_dabt_build_dt.;

data _null_;
input_dttm = &refer_dttm.;
call symput ('refer_dttm',input_dttm);
run;

/*Error handling*/
%let job_rc = 0;
%let err = &syserr;
%if (&err gt &job_rc) %then
%let job_rc = &err;
/*End : Error handling*/
%if %sysfunc(exist(&m_dabt_output_table_libref..&m_dabt_output_table_nm.)) %then %do;
	proc fedsql &m_fedsql_option.; 
		drop table &m_dabt_output_table_libref..&m_dabt_output_table_nm.; 
	quit;
%end;

proc fedsql &m_fedsql_option.;
	create table &m_dabt_output_table_libref..&m_dabt_output_table_nm. as
	select 	&m_dabt_subset_retain_key_tbl_nm..&m_dabt_retained_key_column_nm,
			SQRT(sum(ACCOUNT_snapshot_BASE.balance_amt)) AS I_SQRT_BAL_AMT_l1M
	from &m_dabt_subset_retain_key_libref..&m_dabt_subset_retain_key_tbl_nm. as &m_dabt_subset_retain_key_tbl_nm.
	Inner join BANKCRFM.ACCOUNT_snapshot_BASE as ACCOUNT_snapshot_BASE
	on &m_dabt_subset_retain_key_tbl_nm..CUSTOMER_RK = ACCOUNT_SNAPSHOT_BASE.CUSTOMER_RK
	WHERE ACCOUNT_SNAPSHOT_BASE.PERIOD_LAST_DTTM = &refer_dttm
	group by &m_dabt_subset_retain_key_tbl_nm..CUSTOMER_RK, &m_dabt_subset_retain_key_tbl_nm..&m_dabt_retained_key_column_nm;
quit;

/*Error handling*/
%let err = &syserr;
%if (&err gt &job_rc) %then
%let job_rc = &err;
/*End : Error handling*/
/*If the external code was not executed successfully,
then setting the variable m_dabt_extrnl_calc_success_flg to N.
Application checks if the value of m_dabt_extrnl_calc_success_flg is N.
If yes, then it stops further execution.*/
%if &job_rc. gt 0 %then
%let m_dabt_extrnl_calc_success_flg = N;
%mend calc_sqrt_sum_bal_l1M;
%calc_sqrt_sum_bal_l1M;
