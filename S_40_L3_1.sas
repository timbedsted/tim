
/* Drop output table before creating */
/*m_cas_flg will be derived from csbinit and it is set to Y if it is cas table. */
%dabt_drop_table(m_table_nm=&LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._sk_tmp , m_cas_flg = Y );

%dabt_drop_table(m_table_nm=&LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._sk , m_cas_flg = Y );

%dabt_drop_table(m_table_nm=&LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._rk_tmp , m_cas_flg = Y );

%dabt_drop_table(m_table_nm=&LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._rk , m_cas_flg = Y );


/* Creating rk table containing entire population as of build date, as subset is not specified */

proc fedsql &m_fedsql_option.;
	create table &LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._rk_tmp as
	select CUSTOMER_RK, datepart(cast(FIRST_ACCOUNT_OPEN_DT as timestamp)) as dabt_avg_date, FIRST_ACCOUNT_OPEN_DT as dabt_avg_dttm
	from bankcrfm.CUSTOMER_DIM
	where (&DABT_LOAD_USER_INPUT_DTTM_CAS >= CUSTOMER_DIM.VALID_START_DTTM and &DABT_LOAD_USER_INPUT_DTTM_CAS <= CUSTOMER_DIM.VALID_END_DTTM )
	;
quit;
%dabt_err_chk(type=SQL);

/* Creating rk table containing entire population as of build date, as subset is not specified */

proc fedsql &m_fedsql_option.;
	create table &LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._rk as
	select CUSTOMER_RK, dabt_avg_date , dabt_avg_dttm
	, case when dabt_avg_dttm <= %dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS,-1,'END')
	then 1
	else %dabt_intck("DTMONTH",dabt_avg_dttm,%dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS, -0,'END')) + 1
	end as dabt_avg_dttm_0_1_M
	, case when dabt_avg_dttm <= %dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS,-3,'END')
	then 3
	else %dabt_intck("DTMONTH",dabt_avg_dttm,%dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS, -0,'END')) + 1
	end as dabt_avg_dttm_0_3_M
	, case when dabt_avg_dttm <= %dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS,-2,'END')
	then 1
	else %dabt_intck("DTMONTH",dabt_avg_dttm,%dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS, -1,'END')) + 1
	end as dabt_avg_dttm_1_2_M
	, case when dabt_avg_dttm <= %dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS,-6,'END')
	then 4
	else %dabt_intck("DTMONTH",dabt_avg_dttm,%dabt_intnx("DTMONTH",&DABT_LOAD_USER_INPUT_DTTM_CAS, -2,'END')) + 1
	end as dabt_avg_dttm_2_6_M
	from &LIB_SCR..s_&VAR_TARGET_TABLE_SK._l3_1_&RUN_NO._rk_tmp
	;
quit;
%dabt_err_chk(type=SQL);
