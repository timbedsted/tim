cas;

libname fdm 	cas   caslib=bankcrfm;
libname arm 	cas   caslib=rm_arm;

libname rm_scr 	cas   caslib=rm_scr;
libname rm_ctrl cas   caslib=rm_ctrl;

libname Rm_perf cas   caslib=Rm_perf;

* In Code execution create tbale *;
/*%csbmva_create_parameter_ctrl_tbl;*/



data rm_ctrl.Rm_Control_Parameter(promote=yes);
	set rm_ctrl.Rm_Control_Parameter(drop=parameter_value_dt);
	length parameter_value_dt 8; 
	parameter_value_dt='31DEC2013'd;
	format parameter_value_dt date9.; 
run;

proc print data=rm_ctrl.Rm_Control_Parameter;
run;


proc cas;
table.update/
set={{var="parameter_value_dt",value="'31Mar2012'd"}}
table={caslib="rm_ctrl",name="rm_control_parameter",where="parameter_nm='LATEST_DATE_SOURCE_DATA'"};
quit;



proc freq data=fdm.account_snapshot_base(datalimit=all);
format PERIOD_LAST_DTTM  datetime22.; 
	tables PERIOD_LAST_DTTM / list missing;
run;



** show data in apdm.jobmaster*;
filename output filesrvc folderpath="/Public" filename= "output.html";
ods html file=output;
Proc SQL;
SELECT * from APDM.jobmaster;
Quit;
ods html close;



cas;

libname fdm cas   caslib=bankcrfm;

proc  contents data=fdm.ind_customer_x_account;
run;


proc freq data=fdm.ind_customer_x_account;

tables customer_postal_cd  /list missing;
run;



proc freq data=arm._75_bin_prdctd_rslt  ;
format SCORING_AS_OF_DTTM    datetime22.; 
	tables SCORING_AS_OF_DTTM / list missing;
run;



proc freq data=arm._75_bin_actual_rslt  ;
format SCORING_AS_OF_DTTM  actual_AS_OF_DTTM    datetime22.; 
	tables SCORING_AS_OF_DTTM  actual_AS_OF_DTTM    / list missing;
run;
