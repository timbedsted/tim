/*************************************************************************************************
	NAME:	Exercise_2_6_c.sas
	DESC:	External code example exercise
			Calculate age of customer at build time from customer_dim
*************************************************************************************************/
/*  DEBUG 


cas mysession;
caslib _all_ assign;
data _null_;
	refer_date='31AUG2012'd;
	put refer_date=;
	refer_dttm='31AUG2012:23:59:00'dt;
	put refer_dttm=;
	call symput ('refer_dttm',refer_dttm);
	call symput ('refer_date',refer_date);
run;
%put &refer_dttm. &refer_date.; 

 

proc fedsql SESSREF=MYSESSION;
create table casuser.test as
select   SUBSET_TABLE.customer_rk,
intck('MONTH',datepart(subset_table.INCORPORATION_DT),&refer_date.) AS CALC_CUST_AGE
from bankcrfm.customer_dim as subset_table
Inner join BANKCRFM.CUSTOMER_DIM as customer_dim
on  subset_table.CUSTOMER_RK = customer_dim.CUSTOMER_RK
WHERE cast(customer_dim.valid_start_dttm as double)  >= &refer_dttm.
;
quit;

*/


%macro CALC_CUST_AGE;
	OPTIONS MPRINT  SYMBOLGEN;

	/*Macro variable DABT_LOAD_USER_INPUT_DTTM contains the input datetime.*/
	%let refer_dttm = &m_dabt_build_dttm.;
	/*Macro variable DABT_LOAD_USER_INPUT_DT contains the input date.*/
	%let refer_date = &m_dabt_build_dt.;
	data _null_;
	input_dttm = &refer_dttm.;
	call symput ('refer_dttm',input_dttm);
	run;

	%let job_rc = 0;
	%let err = &syserr;
	%if (&err gt &job_rc) %then
	%let job_rc = &err;
	
	
	/*End : Error handling*/
	%if %sysfunc(exist(&m_dabt_output_table_libref..&m_dabt_output_table_nm.)) %then %do;
	proc fedsql &m_fedsql_option.; 
	drop table &m_dabt_output_table_libref..&m_dabt_output_table_nm.; 
	quit;
	%end;


	proc fedsql &m_fedsql_option.;
		create table &m_dabt_output_table_libref..&m_dabt_output_table_nm. as
		select 	&m_dabt_subset_retain_key_tbl_nm..&m_dabt_retained_key_column_nm,
				%dabt_intck('MONTH',%dabt_func_datepart(&m_dabt_subset_retain_key_tbl_nm..INCORPORATION_DT),&refer_date.) AS CALC_CUST_AGE
		from &m_dabt_subset_retain_key_libref..&m_dabt_subset_retain_key_tbl_nm. as &m_dabt_subset_retain_key_tbl_nm.
		Inner join BANKCRFM.CUSTOMER_DIM as customer_dim
		on &m_dabt_subset_retain_key_tbl_nm..CUSTOMER_RK = customer_dim.CUSTOMER_RK
		WHERE customer_dim.valid_start_dttm  >= &refer_dttm.
		;
	quit;


	/*Error handling*/
	%let err = &syserr;
	%if (&err gt &job_rc) %then
	%let job_rc = &err;
	/*End : Error handling*/
	/*If the external code was not executed successfully,
	then setting the variable m_dabt_extrnl_calc_success_flg to N.
	Application checks if the value of m_dabt_extrnl_calc_success_flg is N.
	If yes, then it stops further execution.*/
	%if &job_rc. gt 0 %then
	%let m_dabt_extrnl_calc_success_flg = N;
%mend CALC_CUST_AGE;


%CALC_CUST_AGE;
