%csbinit();


%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =31JAN2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =31JAN2012 , m_populate_arm_flg = Y);


%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =29FEB2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =29FEB2012 , m_populate_arm_flg = Y);




%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =31MAR2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =31MAR2012 , m_populate_arm_flg = Y);



%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =30APR2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =30APR2012 , m_populate_arm_flg = Y);




%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =31MAY2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =31MAY2012 , m_populate_arm_flg = Y);





%dabt_build_scr_abt_wrapper(m_model_id=75,m_scoring_as_of_dt =30JUN2012, m_perform_scoring_flg = Y , m_populate_arm_flg = Y);


%dabt_build_act_abt_wrapper(m_model_id=75, m_scoring_as_of_dt =30JUN2012 , m_populate_arm_flg = Y);




* check scoring run in APDM *;
filename output filesrvc folderpath="/Public" filename= "scoring_control_detail.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc sql;
select * from apdm.scoring_control_detail;
quit;
/* ===========ACTUAL CODE========================*/
ods html close;


* check scoring run in APDM *;
filename output filesrvc folderpath="/Public" filename= "actual_result_control_detail.html";
ods html file=output;
/* ===========ACTUAL CODE======================== */
proc sql;
select * from apdm.actual_result_control_detail;
quit;
/* ===========ACTUAL CODE========================*/
ods html close;


%csbinit();
%csbmva_ong_mdl_performance_run(model_id=75);



proc sql;
		delete * 
		from apdm.actual_result_control_detail
		where actual_result_abt_nm  contains 'ACT_75_'
		;
quit;



data _null_;
	set rm_ctrl.RM_CONTROL_PARAMETER;
	where parameter_nm="LATEST_DATE_SOURCE_DATA";
	date9=put(parameter_value_dt,date9.);
	call symputx('dt9_load_dttm',date9);
run;


%put &dt9_load_dttm.; 

