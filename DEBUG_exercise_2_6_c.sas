/*********************************************************************************
	NAME:	DEBUG_exercise_2_6_c.sas
	DESC:	Calculate customer Age from Customer dim in external code
***********************************************************************************/

/**********************************************************************************************************
	DESC: Checks SQL return codes in RMD External code
	NAME: post_sql.sas
**********************************************************************************************************/
%macro post_sql; 
	%if &SQLRC = 0 %then %do; 
		%let m_dabt_extrnl_calc_success_flg=Y;
	%end; 
	%else %do; 
		%let m_dabt_extrnl_calc_success_flg=N;
	%end; 

	%put 'SQLRC=' &SQLRC; 
%mend post_sql;  

* get data ready for example *;

libname casuser cas;
cas;
caslib _all_ assign;

data work.customer_dim;
	set bankcrfm.customer_dim;
run;

data work.subset_table;
	set bankcrfm.customer_dim;
	if _n_ gt 250 then stop;
run;

/**********************************************************************************************************
	DESC: Prototype External Code for RMD
	NAME: Extern_code_001.sas

**********************************************************************************************************/
/**/

* Setup (to come from RMD)*;
%let m_dabt_output_table_libref=userdef;			* Name of CAS RMD final libref *;  
%let m_dabt_scratch_table_libref=WORK;   			* CAS RMD Scratch Libref*;
%let m_dabt_subset_retain_key_libref=work;		* CAS libref which contains retain key column table *;
%let m_dabt_subset_retain_key_tbl_nm=subset_table;	* Table name which contains retained key*;

* Fixed vars *;
%let m_dabt_scratch_table_cd=TEST; 					* Varchar(18) prefix for scratch tables *;
%let m_dabt_output_table_nm=ext_vars;    			* Name of final table *;
%let m_dabt_extrnl_var_outcome_flg=N;			* is it an outcome var?*;
%let m_dabt_src_data_time_grain_cd=MONTH;		* Data aggregation level*;
%let m_dabt_retained_key_column_nm=customer_rk; * CUSTOMER_RK/ACCOUNT_RK/.....;


* ETL step 1 OF N *;
proc sql;
	create table &m_dabt_scratch_table_libref..&m_dabt_scratch_table_cd.intmediate_01  as 
		select customer_rk, max(height) as max_height
		from work.class
		group by customer_rk 
	;
quit;

* Check calculation succeed;
%post_sql;

* Join with subset table to get final table *;

proc sql;
	create table  &m_dabt_output_table_libref..&m_dabt_output_table_nm. as
		select a.*
		from  &m_dabt_scratch_table_libref..&m_dabt_scratch_table_cd.intmediate_01  a, &m_dabt_subset_retain_key_libref..&m_dabt_subset_retain_key_tbl_nm. b
		where a.&m_dabt_retained_key_column_nm.=b.&m_dabt_retained_key_column_nm.
	;
quit;

* Check calculation succeed;
%post_sql;
 
	

* What variable were calculated *;
%let m_dabt_variable_to_calc_lst=max_height;


