/***********************************************************
	NAME:	pg_libname.sas
	DESC:	Access the Postgress tables specific for ID

************************************************************/
/*PostgreSQL & CAS tables*/
libname sas_id POSTGRES DATABASE=SharedServices SCHEMA="riskdecisioning"
PRESERVE_COL_NAMES=NO SERVER="20.120.125.236"
PORT=5432 user="dbmsowner" password="ozmuPXUncweERkjQijuoYTWXHnojroFx" DBMAX_TEXT=10000
conopts="sslmode=allow";

%let mysrv1=rmdemo.postgres.database.azure.com;
%let myusr1=azureuser@rmdemo.postgres.database.azure.com;
cas; 
caslib pg desc='PostgreSQL Caslib' 
     dataSource=(srctype='postgres'
                 server="&mysrv1."
                 username="dbmsowner"
                 password='ozmuPXUncweERkjQijuoYTWXHnojroFx'
                 database="SharedServices"
                 schema='riskdecisioning');


caslib _all_ assign;


cas casauto terminate;

